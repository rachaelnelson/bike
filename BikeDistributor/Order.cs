﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BikeDistributor.Receipts;

namespace BikeDistributor
{    
    public class Order
    {              
        private readonly IList<Line> _lines = new List<Line>();

        // It is possible we'd want to add/remove from this list, so leaving
        // as IList instead of IEnumerable
        public IList<LinePrice> linePrices = new List<LinePrice>();

        public string Company { get; private set; }

        // Order total
        public decimal Total { get; set; }
        
        public Order(string company)
        {
            Company = company;
        }        

        public void AddLine(Line line)
        {
            _lines.Add(line);
        }

        // Going off this line in the readme file:
        // "more bikes at new prices, different discount codes and
        // percentages, and/or additional receipt formats."            

        // If the discount/tax was dependent on the type/model of bike, 
        // then I would pass an interface into Bike and call from there, 
        // such as line.Bike.CalculatePrice().
        // Then instantiate that Bike with the proper calculator.

        // However, this calculation looks to be based
        // solely on price and quantity, not bike/model specific
        // so for now build a calculator function.       

        // I do not actually see a "discount code" in the source
        // here so if that's a new feature, that would require
        // more information to implement.  Only a discount percentage
        // is present.              
        private void CalculateLines()
        {            
            linePrices = PriceCalculator.CalculateLinePrices( _lines );
        }

        public string Receipt(ReceiptType receiptType)
        {
            CalculateLines();

            ReceiptContent content = ReceiptConverter.GetReceiptContent( linePrices, Company );

            var receipt = ReceiptBuilder.Build(receiptType);
            return receipt.Generate(content);
        }
    }
}
