﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor.Receipts
{
    public class TextReceipt : Receipt
    {
        public string Generate(ReceiptContent content)
        {            
            var result = new StringBuilder(string.Format("Order Receipt for {0}{1}", content.Company, Environment.NewLine));

            foreach (var line in content.Lines)
            {
                result.AppendLine(string.Format("\t{0} x {1} {2} = {3}", line.Quantity, line.Brand, line.Model, line.Total));
            }

            result.AppendLine(string.Format("Sub-Total: {0}", content.SubTotal));
            result.AppendLine(string.Format("Tax: {0}", content.Tax ));
            result.Append(string.Format("Total: {0}", content.Total));

            return result.ToString();
        }
    }
}
