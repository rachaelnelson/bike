﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor
{
    // Discount thresholds
    // Example  { 1000, 10, .1m }, with .1 being the discount
    //  $100 *.1 = $10 off
    public class Discount
    {
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public decimal Percentage { get; set; }
    }
}
