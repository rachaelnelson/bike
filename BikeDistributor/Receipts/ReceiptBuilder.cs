﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor.Receipts
{
    public static class ReceiptBuilder
    {        
        public static Receipt Build(ReceiptType type)
        {
            var dict = new Dictionary<ReceiptType, Receipt>()
            {
                    { ReceiptType.Text, new TextReceipt() },
                    { ReceiptType.Html, new HtmlReceipt() }
            };

            return dict[type];
        }
    }
}
