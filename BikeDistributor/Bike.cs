﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor
{
    // RN - An interface could be used here to abstract out Bike or to make each of these passed in items objects in themselves.
    // However at the present moment, it doesn't appear that additional complexity is needed,
    // so keeping as is.  Easy refactor to do, if needed down the line.
    public class Bike
    {
        public string Brand { get; private set; }
        public string Model { get; private set; }
        public decimal Price { get; set; }        

        public Bike(string brand, string model, int price)
        {
            Brand = brand;
            Model = model;
            Price = price;
        }       
    }    
}
