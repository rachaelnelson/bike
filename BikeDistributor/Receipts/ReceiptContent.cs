﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor.Receipts
{
    public class ReceiptContent
    {
        public string Company { get; set; }
        public List<ReceiptLine> Lines { get; set; }
        public string Tax { get; set; }
        public string SubTotal { get; set; }
        public string Total { get; set; }
    }
    
    public class ReceiptLine
    {        
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Quantity { get; set; }
        public string Total { get; set; }
    }

    public static class ReceiptConverter
    {
        public static ReceiptContent GetReceiptContent(IList<LinePrice> linePrice, string company)
        {
            ReceiptContent content = new ReceiptContent();
            List<ReceiptLine> receipt = new List<ReceiptLine>();

            foreach( var item in linePrice )
            {
                receipt.Add( new ReceiptLine()
                {
                    Brand = item.line.Bike.Brand,
                    Model = item.line.Bike.Model,
                    Quantity = item.line.Quantity.ToString(),                   
                    Total    = item.PriceAfterDiscount.ToString("c")
                });
            }

            content.Lines    = receipt;

            content.Tax      = linePrice.Sum( x => x.Tax ).ToString("c");
            content.SubTotal = linePrice.Sum(x => x.PriceAfterDiscount).ToString("c");
            content.Total    = linePrice.Sum(x => x.NetPrice).ToString("c");

            content.Company = company;

            return content;
        }
    }
}
