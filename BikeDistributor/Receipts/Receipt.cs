﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor.Receipts
{
    public interface Receipt
    {
        string Generate(ReceiptContent content);
    }
}
