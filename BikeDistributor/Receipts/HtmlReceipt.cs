﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor.Receipts
{
    public class HtmlReceipt : Receipt
    {
        public string Generate(ReceiptContent content)
        {
            var result = new StringBuilder(string.Format("<html><body><h1>Order Receipt for {0}</h1>", content.Company));

            result.Append("<ul>");

            foreach (var line in content.Lines)
            {
                result.Append(string.Format("<li>{0} x {1} {2} = {3}</li>", line.Quantity, line.Brand, line.Model, line.Total));
            }

            result.Append("</ul>");

            result.Append(string.Format("<h3>Sub-Total: {0}</h3>", content.SubTotal));
            result.Append(string.Format("<h3>Tax: {0}</h3>", content.Tax));
            result.Append(string.Format("<h2>Total: {0}</h2>", content.Total));

            result.Append("</body></html>");

            return result.ToString();
        }
    }
}
