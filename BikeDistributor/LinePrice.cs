﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor
{
    public class LinePrice
    {
        // These are good to store in case an adjustment later is needed
        // (cancelled items, authorize.net/cc adjustments, etc)

        // Price before discounts and tax.        
        public decimal ListPrice { get; set; }        
        // Amount of the discount (ex: $30 for example)
        public decimal Discount { get; set; }
        // Amount of tax (ex: $10 tax on a $100 item)
        public decimal Tax { get; set; }

        // Price after discounts
        public decimal PriceAfterDiscount { get {
                // Disallow discounts greater than price
                if (Discount > ListPrice) return 0;

                return ListPrice - Discount;
        }}
        
        // Price after discount and taxes
        public decimal NetPrice { get {
                return PriceAfterDiscount + Tax;
        }}

        public Line line { get; private set; }

        public LinePrice( Line line )
        {
            this.line = line;
        }            
    }
}
