﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeDistributor
{
    // Static until evident there's need for additional Calculators
    // For example, if another item is sold, such as Unicycles,
    // This is fairly easy to abstract out into an interface
    public static class PriceCalculator
    {
        public static IEnumerable<Discount> GetDiscounts()
        {
            // In reality I would store this in a database and load it up.
            // If prices don't change frequently, is a good candidate for caching.
            return new List<Discount>() {               
               new Discount { Price = 1000, Quantity = 20, Percentage = .1m },
               new Discount { Price = 1000, Quantity = 10, Percentage = .05m },
               new Discount { Price = 2000, Quantity = 10, Percentage = .2m },
               new Discount { Price = 5000, Quantity = 5,  Percentage = .2m }
            }
            // lowest price first
            .OrderBy( o => o.Price )
            // start at largest quantity discount and descend
            .ThenByDescending( o => o.Quantity )
            .ToList();
        }

        // TODO:  rounding
        public static IList<LinePrice> CalculateLinePrices( IEnumerable<Line> lines )
        {
            var discounts = GetDiscounts();

            List<LinePrice> priceList = new List<LinePrice>();

            foreach( var line in lines )
            {
                var linePrice = new LinePrice(line);
                linePrice.ListPrice = line.Quantity * line.Bike.Price;
                linePrice.Discount = CalculateDiscount(discounts, line.Bike.Price, line.Quantity);                
                linePrice.Tax      = CalculateTax(linePrice.PriceAfterDiscount);

                priceList.Add( linePrice );
            }

            return priceList;
        }

        // Ex:  10 items @ $10 each = $100
        //      .1 discount = $10
        public static decimal CalculateDiscount(IEnumerable<Discount> discounts, decimal price, int quantity)
        {
            foreach (var discount in discounts)
            {
                if (price == discount.Price && quantity >= discount.Quantity)
                {
                    return (discount.Price * discount.Quantity) * discount.Percentage;
                }
            }

            return 0;
        }

        // This can change per jurisdiction, etc.  
        // Can be refactored later if needed        
        public static decimal GetTaxRate()
        {
            return .0725m;
        }

        public static decimal CalculateTax(decimal price)
        {
            return price * GetTaxRate(); 
        }            
    }   
}
